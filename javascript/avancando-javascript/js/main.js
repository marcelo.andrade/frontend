var list = [
    {desc: 'rice', amount: '1', value: '5.40'},
    {desc: 'bean', amount: '12', value: '1.99'},
    {desc: 'juice', amount: '1', value: '15.00'}
];

function getTotal(list) {
    var total = 0;
    for (let key in list) {
        total += list[key].value * list[key].amount;
    }
    document.getElementById('total').innerHTML = formatValue(total);
}

//for whith counter
/*function getTotal (list) {

    if (list != null) {
        let total= 0;
        let count= list.length;

        for (let i = 0; i < count; i++) {
            total += list[i].value * list[i].amount;
        }
        return total;
    }
}*/

// console.log(getTotal(list));

//Exercise
//Crie um JSON com objetos do tipo produto, que tenha descricao, quantidade e preço. Multiplique todos os registros
//leia os registros em um laço de repetição

const products = [
    {description: 'rice', price: '11.99', amount: 1},
    {description: 'bean', price: '3.99', amount: 3},
    {description: 'juice', price: '1.99', amount: 4}
];

// console.log(products);

function sumProducts(products) {
    if (products != null) {
        let total = 0;
        let count = products.length;

        for (let i = 0; i < count; i++) {
            total += products[i].amount * products[i].price;
        }

        return total;
    }
}

console.log(sumProducts(products));

//function to create table
function setList(list) {
    let table = '<thead><tr><td><strong>Description</strong></td><td><strong>Amount</strong></td><td><strong>Value</strong></td><td><strong>Action</strong></td></tr></thead><tbody>';
    //list in table
    for (let key in list) {
        table += ' <tr><td>' + formatDesc(list[key].desc) + '</td><td>' + formatAmount(list[key].amount) + '</td> <td>' + formatValue(list[key].value) + ' </td><td><button class="btn btn-default" onclick="setUpdate(' + key + ');">Edit</button>  <button class="btn btn-danger" onclick="deleteData(' + key + ');">Delete</button></td></tr>';
    }

    table += '</tbody>';

    document.getElementById('listTable').innerHTML = table;
    getTotal(list);

    saveListStorage(list);
}

//calling the function to create table on screen
// setList(list);

//function to apply the first capital letter
function formatDesc(desc) {
    let str = desc.toLowerCase();
    str = str.charAt(0).toUpperCase() + str.slice(1);
    return str;
}

//function to format value
function formatValue(value) {
    let str = parseFloat(value).toFixed(2) + '';
    str = str.replace('.', ',');
    str = "R$ " + str;
    return str;
}

//function to format amount
function formatAmount(amount) {
    return parseInt(amount);
}

//function addData to add a new register data on the list
function addData() {
    if (!validator()) {
        return;
    }

    const desc = document.getElementById("desc").value;
    const amount = document.getElementById("amount").value;
    const value = document.getElementById("value").value;
    //adding the object to array list
    list.unshift({desc: desc, amount: amount, value: value});

    resetForm();
    //adding on screen
    setList(list);

    document.getElementById('deleteList').removeAttribute('disabled');
}

//function resetForm to reset the data on the screen
function resetForm() {
    document.getElementById('desc').value = "";
    document.getElementById('amount').value = "";
    document.getElementById('value').value = "";

    document.getElementById('btnUpdate').style.display = 'none';
    document.getElementById('btnAdd').style.display = 'inline';

    document.getElementById('inputIDUpdate').innerHTML = '';

    document.getElementById('errors').style.display = 'none';
}

//function setUpdate to edit data register
function setUpdate(key) {
    const obj = list[key];
    document.getElementById('desc').value = formatDesc(obj.desc);
    document.getElementById('amount').value = obj.amount;
    document.getElementById('value').value = (obj.value);

    document.getElementById('btnUpdate').style.display = 'inline';
    document.getElementById('btnAdd').style.display = 'none';

    document.getElementById('inputIDUpdate').innerHTML = '<input type="hidden" id="idUpdate" value="' + key + '">'
}

//function updateData to save a new data register
function updateData() {
    if (!validator()) {
        return;
    }
    const id = document.getElementById('idUpdate').value;

    const desc = document.getElementById('desc').value;
    const amount = document.getElementById('amount').value;
    const value = document.getElementById('value').value;

    console.log(value);

    list[id] = {desc: desc, amount: amount, value: value};

    resetForm();

    setList(list);
}

//function deleteData to delete data registers
function deleteData(key) {
    if (confirm('Delete this register?')) {
        if (key === 0) {
            list.shift();
        } else if (key === list.length - 1) {
            list.pop();
        } else {
            const arrAuxIni = list.slice(0, key);
            const arrAuxEnd = list.slice(key + 1);
            list = arrAuxIni.concat(arrAuxEnd);
        }
        resetForm();
        setList(list);
    }
}

//function to validate form
function validator() {
    var errors = '';

    const desc = document.getElementById('desc').value;
    const amount = document.getElementById('amount').value;
    const value = document.getElementById('value').value;

    //display none if the validation has not alerts
    document.getElementById('errors').style.display = 'none';
    //desc
    if (desc === '') {
        errors +=  '<p><strong>Fill out a Description</strong></p>';
    }

    //amount
    if (amount === '') {
        errors += '<p><strong>Fill out a Amount</strong></p>';
    } else if (amount != parseInt(amount)) {
        errors += '<p><strong>Fill out a valid Amount</strong></p>'
    }

    //value
    if (value === '') {
        errors +=  '<p><strong>Fill out a Value</strong></p>';
    } else if (value != parseFloat(value)) {
        errors += '<p><strong>Fill out a valid Value</strong></p>'
    }

    if (errors !== "") {
        document.getElementById('errors').style.display = 'block';
        document.getElementById('errors').innerHTML = '<span><p>Error</p></span>' + errors;
        return 0;
    } else {
        return 1;
    }
}

//function to delete list
function deleteList() {
    if (confirm('Deseja mesmo deletar a lista de registro?')) {
        list = [];
        setList(list);
        if (list.length <= 0) {
            document.getElementById('deleteList').setAttribute("disabled","disabled");
        }
    }
}

//function to save list
function saveListStorage() {
    const jasonStr = JSON.stringify(list);
    localStorage.setItem('list',jasonStr)
}

//function to init the Application
function initListStorage() {
    const testList = localStorage.getItem('list');
    if (testList) {
        list = JSON.parse(testList);
    }
    setList(list);
}

initListStorage();