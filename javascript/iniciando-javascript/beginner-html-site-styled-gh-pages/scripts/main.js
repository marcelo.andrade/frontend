// //seleciona a tag h1
// var myHeading = document.querySelector('h1');
//
// //modifica/atribui o contexto da tag h1
// myHeading.innerHTML = 'Hello world!';
//
// var number;
// var number1 = 1;
//
// number = "28" + number1;
//
// console.log(number);
//
// console.log(typeof number);
//
// console.log(myHeading.innerHTML);
//
// //funcao com que contem o mesmo nome de varivel anterior
// /*function myFunction() {
//     //seu código aqui
// }
// */
//
// //funcao com que contem o mesmo nome de varivel anterior injetada em variável
// var myFunction = function () {
//     var number = 0.1 + 22;
//     console.log(number);
// };
// //chamada da funcao
// myFunction();
//
// //Aula - Condições
//
// var age = prompt('How old are you?');
//
// var ageParser = parseInt(age);
//
// if (ageParser >= 18) {
//     console.log('You are authorized!');
// } else {
//     console.log('You are not authorized!')
// }
//
// //while
//
// var i = 0;
//
// while (i < 10) {
//     console.log(i);
//     i++;
// }
//
// //Aula - Trabalhando com funções
// function newFunction(name) {
//     console.log('Name =>', name)
// }
//
// newFunction('Marcelo Neves de Andrade');
//
// //funcao autoexecutável
// (function nameFunction(name) {
//     console.log('Name =>', name )
// })('Mariana Rodrigues da Silva');
//
// //funcao com retorno
// var prop = prompt('What is your name?');
//
// function yourName(name) {
//     return name;
// }
//
// console.log(yourName(prop));
//
// //Aula - Objetos
// var prop1 = prompt('What is your first name?');
// var prop2 = prompt('What is your last name?');
// var prop3 = prompt('What is your favorite color?');
//
// //forma antiga
// //object = new Object;
// object = {
//     firstName: prop1,
//     lastName: prop2,
//     favoriteColor: prop3
// };
//
// console.log(JSON.stringify(object));
//
// console.log(object);
//
// object1 = {};
//
// object1.firstName = prop1;
// object1.lastName = prop2;
// object1.favoriteColor = prop3;
//
// console.log(object1);
//
// //Aula - Arrays
// var ft  = ['Marcelo','Loren'];
//
// //adiciona o valor passado no primeiro ídice do array
// ft.unshift('Louise');
// console.log(ft);
//
// //remove o último índice do array
// ft.pop();
// console.log(ft);
//
// //remove o primeiro ídice do array
// ft.shift();
// console.log(ft);
//
// //remove o ídice informado
// ft.splice(1,1);
// console.log(ft);
//
//
//
// //capturar pelo índice
// console.log(ft.indexOf('Marcelo'));
//
// //unir/concatenar arrays
//
// var ft1 = [
//     'Wislley','Welesson'
// ];
//
// var soma = ft.concat(ft1);
//
// console.log(soma);
//
// //
// console.log(JSON.stringify(soma));


//Aula - Interagindo com o DOM - Eventos
function number_val() {
    let number = document.getElementById('number_val').value;
    if (isNaN(number) || (number_val >= 1 && number <= 10)) {
        alert('Is not valid');
    } else {
        alert('Is valid');
    }
    console.log(number);
}

let click = document.getElementById('click_me');

click.addEventListener('click', fnClick);

function fnClick() {
    alert('You clicked me!');
}

// function over(obj) {
//     obj.innerHTML = 'New paragraph';
//     console.log(obj)
// }




//Aula - Interagindo com o DOM - HTML

// const input = document.getElementsByClassName('number_val_input');
// // console.log(input[0].value);
// const btn_generate = document.getElementById('generate_btn');
//
// btn_generate.addEventListener('click', generate);
//
// function generate() {
//     let value = input[0].value;
//
//     for (let i = 0; i < parseInt(value); i++ ) {
//         let inpt = document.createElement('input');
//         inpt.id = '#btn_' + i;
//
//         let body = document.getElementsByTagName('body');
//         body[0].appendChild(inpt);
//     }
// }


//Select with options
// const body1 = document.getElementsByTagName('body');
//
// const select = document.createElement('select');
//
// console.log(body1, select);
//
// body1[0].appendChild(select);
//
// for (let i = 0; i < 10 ; i++ ) {
//     let option = document.createElement('option');
//     option.id = '#id_' + i;
//     option.value = i;
//     option.innerHTML = i;
//
//     select.appendChild(option);
// }



// //Extra - object user
// const user = {
//     name: 'Marcelo',
//     age: 30
// };
//
// user.idade  = 31;
//
// console.log(user.idade);

//Aula - Interagindo com o DOM - CSS

let title  = document.getElementById('h1');

title.style.color = "#fff000";

function over(obj) {
    obj.style.color = 'red';
}

function out(obj) {
    obj.style.color = defaultStatus;
}